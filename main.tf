// Configure the Google Cloud provider
provider "google" {
 credentials = file("gcp-cred.json")
 project     = "reliable-airway-271213"
 region      = "us-west1"
}

// Terraform plugin for creating random ids
resource "random_id" "instance_id" {
 byte_length = 8
}


// A single Compute Engine instance
resource "google_compute_instance" "default" {
 name = element(variable.instance_tags, count)
 machine_type = "e2-medium"
 zone         = "us-west1-a"
 count        = 2

 boot_disk {
   initialize_params {
     image = "rhel-cloud/rhel-7"
   }
 }

// Make sure flask is installed on all new instances for later steps
 //metadata_startup_script = "sudo apt-get update; sudo apt-get install -yq build-essential python-pip rsync; pip install flask"

 network_interface {
   network = "default"

   access_config {
     // Include this section to give the VM an external ip address
   }
 }
}

variable "instance_tags"
{
  type = list
  default = ["aps1", "aps2"]
}

resource "google_spanner_instance" "example" {
  config       = "nam-eur-asia1"
  display_name = "Multi Regional Instance"
  num_nodes    = 2
  labels = {
    "foo" = "bar"
  }
}
